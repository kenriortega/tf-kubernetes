resource "digitalocean_database_user" "user-website" {
  cluster_id = digitalocean_database_cluster.mysql-website.id
  name       = "foobar"
  mysql_auth_plugin = "mysql_native_password"
}

resource "digitalocean_database_cluster" "mysql-website" {
  name       = var.cluster_database_name
  engine     = "mysql"
  version    = "8"
  size       = "db-s-1vcpu-1gb"
  region     = "nyc1"
  node_count = 1
}

resource "digitalocean_database_db" "database-wordpress" {
  cluster_id = digitalocean_database_cluster.mysql-website.id
  name       = "websiteDB"
}