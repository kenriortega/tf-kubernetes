resource "kubernetes_ingress" "website_ingress" {
  metadata {
    name = "website"
    namespace = "applications"
    annotations = {
      "cert-manager.io/cluster-issuer" = "${var.ClusterIssuerName}-clusterissuer"
    }
  }
  
  spec {
    rule {
      host = "${var.website_record}.${var.domain_name}"
      http {
        path {
          backend {
            service_name = "website-wordpress"
            service_port = 80
          }
          path = "/"
        }
      }
    }
    tls {
      hosts = [
        "${var.website_record}.${var.domain_name}"
      ]
      secret_name = "${var.website_record}-cert"
    }
  }
}

resource "kubernetes_ingress" "sftp_ingress" {
  metadata {
    name = "sftp"
    namespace = "applications"
  }

  spec {
    rule {
      host = "${var.ftp_record}.${var.domain_name}"
      http {
        path {
          backend {
            service_name = "sftp"
            service_port = 22
          }
        }
      }
    }
  }
}