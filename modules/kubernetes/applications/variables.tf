variable "host" {}
variable "token" {}
variable "cluster_ca_certificate" {}
variable "wordpress_username" {}
variable "wordpress_password" {}
variable "mysql_hosts" {}
variable "mysql_port" {}
variable "mysql_user" {}
variable "mysql_password" {}
variable "mysql_database" {}