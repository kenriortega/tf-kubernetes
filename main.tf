provider "digitalocean" {
  token = var.do_token
}

provider "kubernetes" {
  host  = module.do-kubernetes.host
  token = module.do-kubernetes.token
  cluster_ca_certificate = module.do-kubernetes.cluster_ca_certificate
}

provider "helm" {
  kubernetes {
    host  = module.do-kubernetes.host
    token =  module.do-kubernetes.token
    cluster_ca_certificate = module.do-kubernetes.cluster_ca_certificate
  }
}

module "do-kubernetes" {
  source = "./modules/digitalocean/kubernetes"
  do_token = var.do_token
  cluster_name = var.cluster_name
  k8s_version = var.k8s_version
}

module "do-mysql" {
  source = "./modules/digitalocean/databases"
  do_token = var.do_token
  cluster_database_name = var.cluster_database_name
}

module "do-bootstrap-site" {
  source = "./modules/kubernetes/applications"
  host = module.do-kubernetes.host
  token = module.do-kubernetes.token
  cluster_ca_certificate = module.do-kubernetes.cluster_ca_certificate
  wordpress_username = var.wordpress_username
  wordpress_password = var.wordpress_password
  mysql_hosts = module.do-mysql.db_host
  mysql_port = module.do-mysql.db_port
  mysql_user = module.do-mysql.db_user
  mysql_password = module.do-mysql.db_password
  mysql_database = module.do-mysql.db_name
  depends_on = [
    module.do-kubernetes,
    module.do-mysql
  ]
}

module "do-autoscale" {
  source = "./modules/kubernetes/autoscale"
  host = module.do-kubernetes.host
  token = module.do-kubernetes.token
  cluster_ca_certificate = module.do-kubernetes.cluster_ca_certificate
  deploymentWebsiteName = module.do-bootstrap-site.deploymentWebsiteName
  depends_on = [
    module.do-bootstrap-site
  ]
}

module "do-domains-records" {
  source = "./modules/digitalocean/domains"
  do_token = var.do_token
  domain_name = var.domain_name
  website_record = var.website_record
  ftp_record = var.ftp_record
  LoadBalancerIP = module.do-bootstrap-site.LoadBalancerIP
  depends_on = [
    module.do-bootstrap-site
  ]
}

module "do-expose-apps" {
  source = "./modules/kubernetes/ingress"
  host = module.do-kubernetes.host
  token = module.do-kubernetes.token
  cluster_ca_certificate = module.do-kubernetes.cluster_ca_certificate
  domain_name = var.domain_name
  website_record = var.website_record
  ftp_record = var.ftp_record
  ClusterIssuerName = module.do-bootstrap-site.ClusterIssuerName
  depends_on = [
    module.do-domains-records
  ]
}